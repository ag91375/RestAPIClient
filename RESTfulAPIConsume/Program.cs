﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using RESTfulAPIConsume.Constants;
using RESTfulAPIConsume.RequestHandlers;
using System;
using RestSharp;
using System.Net.Http;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Text;



namespace RESTfulAPIConsume
{
    class Program
    {
        public class SpotData
        {
            [JsonProperty("x")]
            public List<int> Years { get; set; }
            [JsonProperty("c")]
            public List<decimal> Rates { get; set; }
            [JsonProperty("type")]
            public string SpotType { get; set; }
            //[JsonIgnore]
            //public Tuple<string, decimal> SpotRates {
            //    get { return Tuple.Create(null, 0.0); } 
            //}
        }

        static void RestSharp1()
        {
            var passcode = "da052706d614aa5b40622874c28ef4400ee1d3d733103cf7e40e8dc4dd33bb04";

            var client = new RestClient($"https://uat.citivelocity.com/analytics/eppublic/charting/data/v1?login=cvsym_52&passcode={passcode}");
            var request = new RestRequest();
            request.Method = Method.POST;
            request.Parameters.Clear();

            string tag = "COMMODITIES.SPOT.SPOT_GOLD";

            var payload = new
            {
                startDate = "20170108",
                endDate = "20170114",
                tags = new List<string>
                {
                    "FX.SPOT.EUR.USD.CITI"
                }
            };

            var jsonString = JsonConvert.SerializeObject(payload);

            Console.WriteLine(jsonString);


            request.AddParameter("application/json",
                jsonString,
                ParameterType.RequestBody);


            client.ExecuteAsync(request, response => {

                if (response != null && ((response.StatusCode == System.Net.HttpStatusCode.OK) &&
                                         (response.ResponseStatus == ResponseStatus.Completed)))
                {

                    Console.WriteLine(response.Content);

                    var body = JObject.Parse(response.Content);
                    var result = JsonConvert.DeserializeObject<IDictionary<string, SpotData>>(body["body"].ToString());

                    foreach (var item in result)
                    {
                        Console.WriteLine($"Item: {item.Key}");

                        var spotData = item.Value;

                        Console.WriteLine($"c={string.Join(", ", spotData.Years)}");
                        Console.WriteLine($"x={string.Join(", ", spotData.Rates)}");
                        Console.WriteLine($"type={spotData.SpotType}");
                        Console.WriteLine();
                        Console.WriteLine();
                    }

                    Console.WriteLine("\n"+response.StatusCode);
                    Console.WriteLine("\n" + response.StatusDescription);
                    Console.WriteLine("\n" + response.ResponseStatus);

                }
                else if (response != null)
                {
                    Console.WriteLine(response.StatusCode);
                    Console.WriteLine(response.StatusDescription);
                    Console.WriteLine(response.ResponseStatus);
                    Console.WriteLine(response.ErrorMessage);
                    Console.WriteLine(response.ErrorException);

                    MessageBox.Show(string.Format
                    ("Status code is {0} ({1}); response status is {2}",
                        response.StatusCode, response.StatusDescription, response.ResponseStatus));
                }

            });
        }

        static void Main(string[] args)
        {
            RestSharp1();
            Console.WriteLine("\n\n\n");    
            Console.ReadLine();
        }

        public static JToken GetReleases(IRequestHandler requestHandler)
        {
            return requestHandler.GetReleases(RequestConstants.Url);
        }
    }
}
